<?php

declare(strict_types=1);

namespace Talentry\CryptographyBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Talentry\Cryptography\Encryption\EncryptionService;
use Talentry\Cryptography\Hashing\HashGenerator;

class CryptographyExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $configuration = $this->processConfiguration(new Configuration(), $configs);

        $hashGenerator = $container->getDefinition(HashGenerator::class);
        $hashGenerator->setArgument('$key', $configuration['hash_key']);

        $encryptionService = $container->getDefinition(EncryptionService::class);
        $encryptionService->setArgument('$encryptionKey', $configuration['encryption_key']);

        if ($container->getParameter('kernel.environment') === 'test') {
            $hashGenerator->setPublic(true);
            $encryptionService->setPublic(true);
        }
    }
}
