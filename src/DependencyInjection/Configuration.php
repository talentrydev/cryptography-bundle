<?php

declare(strict_types=1);

namespace Talentry\CryptographyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    private const string ROOT_NODE = 'cryptography';

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::ROOT_NODE);

        $treeBuilder
            ->getRootNode()
            ->children()
                ->scalarNode('encryption_key')->isRequired()->end()
                ->scalarNode('hash_key')->isRequired()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
