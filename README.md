# Cryptography Bundle

This is a Symfony bundle used for integrating talentrydev/cryptography library into a Symfony project.

## Installing

* Run:

```
composer require talentrydev/cryptography-bundle
```

* Add the CryptographyBundle to your kernel's `registerBundles` method:

```
return [
    //...
    new \Talentry\CryptographyBundle\CryptographyBundle();
];
```

## Configuring

Bundle exposes the following configuration:

| Option         | Default value | Required | Description                                                                                                           |
|----------------|---------------|----------|-----------------------------------------------------------------------------------------------------------------------|
| encryption_key | -             | yes      | Key used for encrypting strings. It must be generated using EncryptionServiceFactory::generateEncryptionKey() method. |
| hash_key       | -             | yes      | Key for generating hashes. It can be any string between 16 and 64 characters in length.                               |

## Development

- Install dependencies: `make deps`
- Run tests: `make test`
