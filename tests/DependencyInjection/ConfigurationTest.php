<?php

declare(strict_types=1);

namespace Talentry\CryptographyBundle\Tests\DependencyInjection;

use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Talentry\Cryptography\Encryption\EncryptionService;
use Talentry\Cryptography\Encryption\Sodium\SodiumEncryptionService;
use Talentry\Cryptography\Hashing\HashGenerator;
use Talentry\Cryptography\Hashing\Sodium\SodiumHashGenerator;

class ConfigurationTest extends KernelTestCase
{
    private const string HASH_KEY = 'abcdefghijklmnopqrstuvwxyz';
    private const string ENCRYPTION_KEY = '9202e7df7bb783b904f72186998240e40ac68c64ac02e19c65fa9a18be1925c9';

    protected function setUp(): void
    {
        putenv('HASH_KEY=' . self::HASH_KEY);
        putenv('ENCRYPTION_KEY=' . self::ENCRYPTION_KEY);
        self::bootKernel();
    }

    public function testHashGeneratorIsCorrectlyConfigured(): void
    {
        /** @var HashGenerator $hashGenerator */
        $hashGenerator = self::getContainer()->get(HashGenerator::class);
        self::assertInstanceOf(SodiumHashGenerator::class, $hashGenerator);
        self::assertSame(self::HASH_KEY, $this->getPrivatePropertyValue($hashGenerator, 'key'));
    }

    public function testEncryptionServiceIsCorrectlyConfigured(): void
    {
        /** @var EncryptionService $encryptionService */
        $encryptionService = self::getContainer()->get(EncryptionService::class);
        self::assertInstanceOf(SodiumEncryptionService::class, $encryptionService);
        self::assertSame(
            self::ENCRYPTION_KEY,
            bin2hex($this->getPrivatePropertyValue($encryptionService, 'encryptionKey')),
        );
    }

    private function getPrivatePropertyValue(object $object, string $property): mixed
    {
        $rc = new ReflectionClass($object);
        $prop = $rc->getProperty($property);
        $prop->setAccessible(true);

        return $prop->getValue($object);
    }
}
